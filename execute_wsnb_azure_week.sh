#!/bin/bash

#Get the information about the as argument
WEEK=$1


#Creating directory for the week
mkdir results_wsnb_azure/week_"$WEEK"/
#Creating the output directory
mkdir results_wsnb_azure/week_"$WEEK"/output
#Creating the results directory
mkdir results_wsnb_azure/week_"$WEEK"/results

python3 scripts/generate_deploy.py 2 azure "$WEEK"

#Code to run the simulations
clear && clear && simulationmain input/platform/homogeneousGrid5000Pstate.xml input/deploy/deploy_wsnb_azure/week_"$WEEK".xml input/workload/azure_2020.txt  --log=root.app:file:results_wsnb_azure/week_"$WEEK"/output/logfile.log

#cd results_wsnb_azure
#pwd

#Script for pre-processing the data
#python3 ../scripts/extract_mig_data.py 