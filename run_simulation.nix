{
  tinypkgs ? import (fetchTarball https://gitlab.com/migvasc/phd_nix_pkgs/-/archive/master/packages-repository-09793e4289fcb80064e97b38d252fff1923660bd.tar.gz) {}
}:

tinypkgs.pkgs.mkShell rec {
  buildInputs = [
    tinypkgs.simulations_batteries
    tinypkgs.migrations_no_congestion
  ];
}
