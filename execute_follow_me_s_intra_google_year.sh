#!/bin/bash
NUMBER_WEEKS=25

#Creating the directory for this simulation
rm -rf "results_follow_me_s_intra_google"
#Creating the directory for this simulation
mkdir "results_follow_me_s_intra_google"

for i in $(seq 0 $NUMBER_WEEKS)
    do bash execute_follow_me_s_intra_google_week.sh $i &
done

wait
echo '##########################################################'
echo 'All simulations finished!'
