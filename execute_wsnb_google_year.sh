#!/bin/bash
NUMBER_WEEKS=52

#Creating the directory for this simulation
rm -rf "results_wsnb_google"
#Creating the directory for this simulation
mkdir "results_wsnb_google"

for i in $(seq 0 $NUMBER_WEEKS)
    do bash execute_wsnb_google_week.sh $i &
done

wait
echo '##########################################################'
echo 'All simulations finished!'
