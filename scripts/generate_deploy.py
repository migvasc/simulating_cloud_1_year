import sys
import shutil
import os

if sys.version_info[0] < 3: 
    from StringIO import StringIO
else:
    from io import StringIO
    
def createDeployFile(baseline,baseline_and_workload,week):
    deployment_contents = StringIO("""<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "https://simgrid.org/simgrid.dtd">
<platform version="4.1">
  <actor host="adonis-1.grenoble.grid5000.fr" function="manager">  
    <!-- n_eval value -->
    <argument value="20"/>    
    <!-- number of DCs -->
    <argument value="9"/>
    <!-- name of DC (netzones) on which the workers are running -->                
    <argument value="grenoble.grid5000.fr"/>
    <argument value="rennes.grid5000.fr"/>
    <argument value="sophia.grid5000.fr"/>    
    <argument value="lyon.grid5000.fr"/>      
    <argument value="toulouse.grid5000.fr"/>        
    <argument value="nancy.grid5000.fr"/>
    <argument value="reims.grid5000.fr"/>
    <argument value="luxembourg.grid5000.fr"/>
    <argument value="lille.grid5000.fr"/>     
    <!-- number of pv panels per DC -->                
    <argument value="164"/>
    <argument value="171"/>  
    <argument value="179"/>      
    <argument value="154"/>
    <argument value="185"/>
    <argument value="281"/>    
    <argument value="75"/>
    <argument value="71"/>    
    <argument value="118"/>
    
    <!-- respective input files per dc -->                
    <argument value="input/photo-voltaic/Canberra/weeks/week_"""+week+"""" />
    <argument value="input/photo-voltaic/Pune/weeks/week_"""+week+"""" />
    <argument value="input/photo-voltaic/Johanesbourg/weeks/week_"""+week+"""" />
    <argument value="input/photo-voltaic/US/weeks/week_"""+week+"""" />
    <argument value="input/photo-voltaic/SP/weeks/week_"""+week+"""" />
    <argument value="input/photo-voltaic/Dubai/weeks/week_"""+week+"""" />
    <argument value="input/photo-voltaic/Singapore/weeks/week_"""+week+"""" />
    <argument value="input/photo-voltaic/Paris/weeks/week_"""+week+"""" />
    <argument value="input/photo-voltaic/Seoul/weeks/week_"""+week+"""" />
    

    <!-- The path of the CSV file containing the historic 
      (in the format {(time,power)}*) of the green power 
      production of any DC (used to compute the standard 
      deviation of the green power productions). -->
    <argument value="input/photo-voltaic/Perfect-Trace-Model.csv"/>	
    
    <!-- Parameter to chose the scheduling strategy for the VMs.
    
    0: NEMESIS
    1: NEMESIS NEW MIGRATIONS
    2: Baseilne 1 - https://doi.org/10.1016/j.jpdc.2019.09.015
    3: Baseline 2 (INTRA DCS MIGS) - https://doi.org/10.1016/j.jss.2021.110907
    4: Baseline 2 (INTER DCS MIGS) - https://doi.org/10.1016/j.jss.2021.110907
    5: Scorpious 
    -->
    <argument value="0"""+baseline+""""/>
    <!-- Batteries parameters: -->                        

    <!-- Max Depth of Discharge (in %): -->        
    <argument value="0.8"/>	

    <!-- Charge efficiency (in %): -->        
    <argument value="0.85"/>	

    <!-- Discharge efficiency (in %): -->        
    <argument value="0.85"/>	

    <!-- Capacity in Wh  ( it will be multiplied by the number of servers in each DC). -->        
    <argument value="3300"/>	
  </actor>
  <!-- The master actor (with some arguments) -->
  <actor host="adonis-1.grenoble.grid5000.fr" function="vmdispatcher"/>      

  <actor host="adonis-1.grenoble.grid5000.fr" function="electricgrid">  
    <!-- number of DCs -->
    <argument value="9"/>

    <!-- name of DC (netzones) on which the workers are running -->                
    <argument value="grenoble.grid5000.fr"/>
    <argument value="rennes.grid5000.fr"/>
    <argument value="sophia.grid5000.fr"/>    
    <argument value="lyon.grid5000.fr"/>      
    <argument value="toulouse.grid5000.fr"/>        
    <argument value="nancy.grid5000.fr"/>
    <argument value="reims.grid5000.fr"/>
    <argument value="luxembourg.grid5000.fr"/>
    <argument value="lille.grid5000.fr"/>     
    <!-- number of pv panels per DC -->                            
    <argument value="164"/>
    <argument value="171"/>  
    <argument value="179"/>      
    <argument value="154"/>
    <argument value="185"/>
    <argument value="281"/>    
    <argument value="75"/>
    <argument value="71"/>    
    <argument value="118"/>    
    <!-- respective input files per dc -->                
    <argument value="input/photo-voltaic/Canberra/weeks/week_"""+week+"""" />
    <argument value="input/photo-voltaic/Pune/weeks/week_"""+week+"""" />
    <argument value="input/photo-voltaic/Johanesbourg/weeks/week_"""+week+"""" />
    <argument value="input/photo-voltaic/US/weeks/week_"""+week+"""" />
    <argument value="input/photo-voltaic/SP/weeks/week_"""+week+"""" />
    <argument value="input/photo-voltaic/Dubai/weeks/week_"""+week+"""" />
    <argument value="input/photo-voltaic/Singapore/weeks/week_"""+week+"""" />
    <argument value="input/photo-voltaic/Paris/weeks/week_"""+week+"""" />
    <argument value="input/photo-voltaic/Seoul/weeks/week_"""+week+"""" />
    <!-- Name of the outputFolder-->
    <argument value="results_"""+baseline_and_workload+"""/week_"""+week+"""/output/"/>
  </actor>  
</platform>
""")
    folder = '.'
    new_directory = 'input/deploy/deploy_'+baseline_and_workload+'/'
    path = os.path.join(folder, new_directory)
    os.makedirs(path,exist_ok = True)


    new_file =new_directory+ 'week_'+week+'.xml'
    with open(new_file, 'w') as fd:
        deployment_contents.seek(0)
        shutil.copyfileobj(deployment_contents, fd)

    return 1


baseline = sys.argv[1]
workload = sys.argv[2]
week = sys.argv[3]

baseline_name =""
#    0: NEMESIS
#    1: NEMESIS NEW MIGRATIONS
#    2: Baseilne 1 - https://doi.org/10.1016/j.jpdc.2019.09.015
#    3: Baseline 2 (INTRA DCS MIGS) - https://doi.org/10.1016/j.jss.2021.110907
#    4: Baseline 2 (INTER DCS MIGS) - https://doi.org/10.1016/j.jss.2021.110907
#    5: Scorpious 

if(baseline == '0'):
  baseline_name = "nemesis"
elif(baseline == '1'):
  baseline_name = "cnemesis"
elif(baseline == '2'):
  baseline_name = "wsnb"
elif(baseline == '3'):
  baseline_name = "follow_me_s_intra"  
elif(baseline == '4'):
  baseline_name = "follow_me_s_inter"  
elif(baseline == '5'):
  baseline_name = "scorpious"    

outputfolder = baseline_name+"_"+ workload
createDeployFile(baseline,outputfolder,week)
