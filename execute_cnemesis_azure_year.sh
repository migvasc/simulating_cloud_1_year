#!/bin/bash
NUMBER_WEEKS=52

#Creating the directory for this simulation
rm -rf "results_cnemesis_azure"
#Creating the directory for this simulation
mkdir "results_cnemesis_azure"

for i in $(seq 0 $NUMBER_WEEKS)
    do bash execute_cnemesis_azure_week.sh $i &
done

wait
echo '##########################################################'
echo 'All simulations finished!'
