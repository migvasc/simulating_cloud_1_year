#!/bin/bash
NUMBER_WEEKS=52

#Creating the directory for this simulation
rm -rf "results_wsnb_azure"
#Creating the directory for this simulation
mkdir "results_wsnb_azure"

for i in $(seq 0 $NUMBER_WEEKS)
    do bash execute_wsnb_azure_week.sh $i &
done

wait
echo '##########################################################'
echo 'All simulations finished!'
