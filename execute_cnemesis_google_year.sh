#!/bin/bash
NUMBER_WEEKS=52

#Creating the directory for this simulation
rm -rf "results_cnemesis_google"
#Creating the directory for this simulation
mkdir "results_cnemesis_google"

for i in $(seq 0 $NUMBER_WEEKS)
    do bash execute_cnemesis_google_week.sh $i &
done

wait
echo '##########################################################'
echo 'All simulations finished!'
