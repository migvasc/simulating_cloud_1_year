#!/bin/bash
NUMBER_WEEKS=52

#Creating the directory for this simulation
rm -rf "results_follow_me_s_inter_azure"
#Creating the directory for this simulation
mkdir "results_follow_me_s_inter_azure"

for i in $(seq 26 $NUMBER_WEEKS)
    do bash execute_follow_me_s_inter_azure_week.sh $i &
done

wait
echo '##########################################################'
echo 'All simulations finished!'
