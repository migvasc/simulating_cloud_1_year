#!/bin/bash

#Get the information about the as argument
WEEK=$1


#Creating directory for the week
mkdir results_wsnb_google/week_"$WEEK"/
#Creating the output directory
mkdir results_wsnb_google/week_"$WEEK"/output
#Creating the results directory
mkdir results_wsnb_google/week_"$WEEK"/results

python3 scripts/generate_deploy.py 2 google "$WEEK"

#Code to run the simulations
clear && clear && simulationmain input/platform/homogeneousGrid5000Pstate.xml input/deploy/deploy_wsnb_google/week_"$WEEK".xml input/workload/google_2011.txt  --log=root.app:file:results_wsnb_google/week_"$WEEK"/output/logfile.log

#cd results_wsnb_google
#pwd

#Script for pre-processing the data
#python3 ../scripts/extract_mig_data.py 